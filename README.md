# Assignment 1 
*The first Java assignment for the Experis Academy program.*

### Characters
The assignment was to build a console application in Java.
There are four types of characters (Warrior, Range, Mage and Rogue) with various types of attributes. 
These attributes increase at different rates as the characters gain levels.
The character can also equip items (weapons and armor). The equipped items 
alter the character's power. 

### Technologies used
The unit tests were written with Junit5.

JDK 16 was used to build the console application.
