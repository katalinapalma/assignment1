package com.company.exceptions;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String invalidArmorMessage) {
        super(invalidArmorMessage);
    }
}