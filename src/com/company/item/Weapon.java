package com.company.item;

/**
 * A class called Weapon that extends the Item class.
 * The properties for this class are: weaponType, damage, attackSpeed and dps.
 * @author Katalina Palma
 */
public class Weapon extends Item {
    protected WeaponType weaponType;
    public int damage;
    public double attackSpeed;
    public double dps;

    /**
     * The Weapon constructor that calls the Item class constructor.
     * Also includes the weaponType, damage, attackSpeed and calls the calculateDPS method.
     */
    public Weapon(WeaponType weaponType, String name, int requiredLevel, Slot slot, int damage, double attackSpeed) {
        super(name, requiredLevel, slot);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        calculateDPS();
    }

    public double calculateDPS() {
        return this.dps = this.damage * this.attackSpeed;
    }
/*
    public double getDps() {
        return dps;
    }

    public void setDps(double dps) {
        this.dps = dps;
    }*/

    public WeaponType getWeaponType() {
        return weaponType;
    }

}
