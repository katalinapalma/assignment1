package com.company.item;

import com.company.character.PrimaryAttribute;

/**
 * A class called Armor that extends the Item class.
 * The properties for this class are: armorType and attributes.
 * @author Katalina Palma
 */
public class Armor extends Item{
    private ArmorType armorType;
    public PrimaryAttribute attributes;

    /**
     * The Armor constructor that takes in armorType, name, requiredLevel, slot and attributes
     */
    public Armor(ArmorType armorType, String name, int requiredLevel, Slot slot, PrimaryAttribute attributes) {
        super(name, requiredLevel, slot);
        this.armorType = armorType;
        this.attributes = attributes;
    }

    public ArmorType getArmorType() {
        return this.armorType = armorType;
    }
}
