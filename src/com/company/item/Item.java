package com.company.item;

/**
 * An abstract super class called Item.
 * The properties for this class are: name, requiredLevel and slot to equip weapon/armor.
 * @author Katalina Palma
 */
abstract public class Item {
    protected String name;
    protected int requiredLevelToEquipItem;
    protected Slot slot;

    /**
     * The Item constructor with name, requiredLevel and slot parameters.
     */
    public Item(String name, int requiredLevel, Slot slot) {
        this.name = name;
        this.requiredLevelToEquipItem = requiredLevel;
        this.slot = slot;
    }

    public int getRequiredLevelToEquipItem() {
        return requiredLevelToEquipItem;
    }

    public Slot getSlot() {
        return slot;
    }
}
