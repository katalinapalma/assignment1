package com.company;
import com.company.character.*;

import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.item.*;
public class Main {

    public static void main(String[] args) {
        var plate = new Armor(ArmorType.PLATE,"Plate", 1, Slot.BODY, new PrimaryAttribute(5, 5,5, 5));
        var hammer = new Weapon(WeaponType.HAMMER, "Eld", 1, Slot.BODY, 5, 5);
        var warrior = new Warrior("Warrior");
        var mage = new Mage("Smoke");
        var wand = new Weapon(WeaponType.WAND, "Raven's eye", 1, Slot.BODY, 2, 1.5);
        var cloth = new Armor(ArmorType.CLOTH, "Fire", 1, Slot.BODY, new PrimaryAttribute(2, 2, 2, 2));

        try {
            System.out.println(mage.characterStats());
            mage.equipWeapon(wand);
            System.out.println("mage: " + mage.characterStats());
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
        try {
            mage.equipArmor(cloth);
            System.out.println(mage.characterStats());
        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }

        try {
            System.out.println(warrior.characterStats());
            warrior.equipWeapon(hammer);
            System.out.println(warrior.characterStats());
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }

        try {
            warrior.equipArmor(plate);

        } catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }

        try {
            warrior.equipWeapon(hammer);
        } catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
    }
}
