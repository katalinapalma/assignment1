package com.company.character;

/**
 * A class containing the primary attributes that each character have.
 * The primary attributes are: vitality, strength, dexterity and intelligence.
 * @author Katalina Palma
 */
public class PrimaryAttribute {
    protected int vitality;
    protected int strength;
    protected int dexterity;
    protected int intelligence;

    public void setAttributes(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public PrimaryAttribute(int vitality, int strength, int dexterity, int intelligence) {
        this.setAttributes(vitality, strength, dexterity, intelligence);
    }

    public int getVitality() {
        return vitality;
    }
    public int getStrength() {
        return strength;
    }
    public int getDexterity() {
        return dexterity;
    }
    public int getIntelligence() {
        return intelligence;
    }
}
