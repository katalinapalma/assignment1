package com.company.character;

/**
 * A class containing the secondary attributes that each character have.
 * This class includes health, armorRating and elementResistance.
 * @author Katalina Palma
 */
public class SecondaryAttribute {
    public int health;
    protected int armorRating;
    protected int elementResistance;

    public SecondaryAttribute(int health, int armorRating, int elementResistance) {
        this.health = health;
        this.armorRating = armorRating;
        this.elementResistance = elementResistance;
    }

    public int getHealth() {
        return health;
    }

    public int getArmorRating() {
        return armorRating;
    }

    public int getElementResistance() {
        return elementResistance;
    }
}
