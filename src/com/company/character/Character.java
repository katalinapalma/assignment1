package com.company.character;
import java.util.HashMap;

import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.item.*;

/**
 * An abstract super class called Character.
 * Properties for this class are: name, level, basePrimaryAttribtues,
 * totalPrimaryAttributes, totalSecondaryAttributes, damageTaken and
 * a hashmap for slot and item.
 * @author Katalina Palma
 */
abstract public class Character {
    protected String name;
    protected int level;
    protected PrimaryAttribute basePrimaryAttributes;
    public int totalPrimaryAttribute;
    protected SecondaryAttribute totalSecondaryAttributes;
    public double damageTaken;
    protected HashMap<Slot, Item> equipment = new HashMap<Slot, Item>();

    public Character(String name) {
        this.name = name;
        this.level = 1;
        this.damageTaken = 1;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return this.level = level;
    }

    public PrimaryAttribute getBasePrimaryAttributes() {
        return basePrimaryAttributes;
    }

    public SecondaryAttribute getTotalSecondaryAttributes() {
        return totalSecondaryAttributes;
    }

    public double getDamageTaken() {
        return damageTaken;
    }

    public void setDamageTaken(double damageTaken) {
        this.damageTaken = damageTaken;
    }

    /**
     * This method calculates the secondary attributes.
     * These attributes are calculated from the primary attributes
     * (vitality, strength, dexterity and intelligence)
     */
    public void calculateTotalSecondaryAttributes() {
        int health = basePrimaryAttributes.vitality * 10;
        int armorRating = basePrimaryAttributes.strength + basePrimaryAttributes.dexterity;
        int elementResistance = basePrimaryAttributes.intelligence;

        totalSecondaryAttributes = new SecondaryAttribute(health, armorRating, elementResistance);
    }

    /**
     * This method calculates the total primary attributes.
     * Here, the primary attributes are added together and result in
     * totalPrimaryAttribute.
     */
    public void calculateTotalPrimaryAttributes() {
        int totalVitality = basePrimaryAttributes.vitality;
        int totalStrength = basePrimaryAttributes.strength;
        int totalDexterity = basePrimaryAttributes.dexterity;
        int totalIntelligence = basePrimaryAttributes.intelligence;

        totalPrimaryAttribute = totalVitality + totalIntelligence+ totalDexterity + totalStrength;
    }

    //All character subclasses require these methods
    abstract void levelUp(int level);
    abstract void calculateDamage();
    abstract boolean equipWeapon(Weapon weapon) throws InvalidWeaponException;
    abstract boolean equipArmor(Armor armor) throws InvalidArmorException;

    //Display character statistics
    public String characterStats() {
        StringBuilder sb = new StringBuilder();

        sb.append("Character name: " ).append(getName());
        sb.append("\nLevel: ").append(getLevel());
        sb.append("\nStrength: ").append(basePrimaryAttributes.strength);
        sb.append("\nDexterity: ").append(basePrimaryAttributes.dexterity);
        sb.append("\nIntelligence: ").append(basePrimaryAttributes.intelligence);
        sb.append("\nHealth: ").append(totalSecondaryAttributes.health);
        sb.append("\nArmor rating: ").append(totalSecondaryAttributes.armorRating);
        sb.append("\nElemental Resistance: ").append(totalSecondaryAttributes.elementResistance);
        sb.append("\nDPS: ").append(getDamageTaken());

        return sb.toString();
    }
}