package com.company.character;

import com.company.exceptions.InvalidArmorException;
import com.company.exceptions.InvalidWeaponException;
import com.company.item.*;

import java.util.Arrays;

/**
 * A sub class for Mage that extends the super class Character
 * Follows same structure as the Warrior class where there are more
 * comments describing the methods used.
 * @author Katalina Palma
 */
public class Mage extends Character {
    protected WeaponType[] validWeaponTypes = new WeaponType[]{WeaponType.STAFF, WeaponType.WAND};
    protected ArmorType[] validArmorTypes = new ArmorType[]{ArmorType.CLOTH};

    public Mage(String name) {
        super(name);
        basePrimaryAttributes = new PrimaryAttribute(5, 1, 1, 8);
        calculateTotalSecondaryAttributes();
    }

    public void levelUp(int level) {
        this.level += level;

        if(level > 0) {
            int newVitality = basePrimaryAttributes.vitality += (3 * level);
            int newStrength = basePrimaryAttributes.strength += (1 * level);
            int newDexterity = basePrimaryAttributes.dexterity += (1 * level);
            int newIntelligence = basePrimaryAttributes.intelligence += (5 * level);

            basePrimaryAttributes.setAttributes(newVitality, newStrength, newDexterity, newIntelligence);
            calculateTotalSecondaryAttributes();
            calculateTotalPrimaryAttributes();
            calculateDamage();
        } else {
            throw new IllegalArgumentException();
        }
    }

    void calculateDamage() {
        double damage = getDamageTaken();
        int intelligence = getBasePrimaryAttributes().getIntelligence();
        setDamageTaken(damage + (intelligence * 1.01));
    }

    @Override
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        Slot slot = weapon.getSlot();
        int requiredLevel = weapon.getRequiredLevelToEquipItem();
        boolean isValidWeapon = Arrays.asList(validWeaponTypes).contains(weapon.getWeaponType());

        if(isValidWeapon && level == requiredLevel) {
            equipment.put(slot, weapon);
            return true;
        } else {
            throw new InvalidWeaponException("You cannot equip this weapon!");
        }
    }

    @Override
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        Slot slot = armor.getSlot();
        int requiredLevel = armor.getRequiredLevelToEquipItem();
        boolean isValidArmor = Arrays.asList(validArmorTypes).contains(armor.getArmorType());

        if(isValidArmor && level == requiredLevel) {
            equipment.put(slot, armor);
            basePrimaryAttributes.vitality += armor.attributes.vitality;
            basePrimaryAttributes.strength += armor.attributes.strength;
            basePrimaryAttributes.dexterity += armor.attributes.dexterity;
            basePrimaryAttributes.intelligence += armor.attributes.intelligence;
            return true;
        } else {
            throw new InvalidArmorException("You cannot equip this armor!");
        }
    }
}